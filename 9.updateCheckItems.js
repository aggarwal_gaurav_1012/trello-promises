const fetch = require('node-fetch');
const apiKey = '302d0bd906ba164038364ba764f6f413';
const apiToken = 'ATTA71b895ec11b616f6ad63f8a7ca27a65db6ee722415ffaf86bd7665401ad3e9b47ADD2EE5';

function updateAllCheckItem(boardId) {
    return getAllCheckList(boardId)
        .then((checkListData) => {
            let checkListIdArray = checkListData.reduce((prevData, currData) => {
                let newCurrData = [];
                newCurrData.push(currData.id);
                newCurrData.push(currData.idCard);
                prevData.push(newCurrData);
                return prevData;
            }, []);
            return checkListIdArray;
        })
        .then((checkListIdArray) => {
            return Promise.all(checkListIdArray.map((id) => getCheckItemsData(id)));
        })
        .catch(error => console.error('Error:', error));
}

function getAllCheckList(boardId) {
    return fetch(`https://api.trello.com/1/boards/${boardId}/checklists?key=${apiKey}&token=${apiToken}`, {
        method: "GET",
    })
        .then((response) => {
            return response.json();
        })
        .catch(error => console.error('Error:', error));
}

function getCheckItemsData(checkListId) {
    return fetch(`https://api.trello.com/1/checklists/${checkListId[0]}/checkItems?key=${apiKey}&token=${apiToken}`, {
        method: "GET",
    })
        .then((response) => {
            return response.json();
        })
        .then((checkItemData) => {
            return Promise.all(checkItemData.map((data) => checkItemsAll(data.id, checkListId[1])));
        })
        .catch(error => console.error('Error:', error));
}

function checkItemsAll(itemsId, cardId) {
    return fetch(`https://api.trello.com/1/cards/${cardId}/checkItem/${itemsId}?state=complete&key=${apiKey}&token=${apiToken}`, {
        method: 'PUT'
    })
        .then(response => {
            return response.json();
        })
        .catch(error => console.error('Error:', error));
}

module.exports = updateAllCheckItem;