// Importing 1.getBoardId.js
const getBoard = require('../1.getBoardId.js');

const boardId = '663082c6d30a82250d9b5cd0';

getBoard(boardId)
    .then(boardData => console.log('Board Data:', boardData))
    .catch(error => console.error('Error:', error));