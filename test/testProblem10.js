const updateAllCheckItem = require('../10.updateCheckItemsIncomplete');

const boardId = '66331a21a16941c5f5727575';

updateAllCheckItem(boardId)
    .then(() => {
        console.log('All checkitems updated to incompleted status successfully');
    })
    .catch(err => console.error(err));