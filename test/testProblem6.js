const { createBoard, createLists, createCards } = require('../6.createBoardListCard');

const apiKey = '302d0bd906ba164038364ba764f6f413';
const apiToken = 'ATTA71b895ec11b616f6ad63f8a7ca27a65db6ee722415ffaf86bd7665401ad3e9b47ADD2EE5';
const boardName = 'New Board';
const listNames = ['List 1', 'List 2', 'List 3'];
const cardNames = ['Card 1', 'Card 2', 'Card 3'];

createBoard(apiKey, apiToken, boardName)
    .then(board => {
        console.log('Board created:', board);
        const boardId = board.id;
        return createLists(apiKey, apiToken, boardId, listNames);
    })
    .then(lists => {
        console.log('Lists created:', lists);
        const listIds = lists.map(list => list.id);
        return createCards(apiKey, apiToken, listIds, cardNames);
    })
    .then(cards => {
        console.log('Cards created:', cards);
    })
    .catch(err => console.error(err));
