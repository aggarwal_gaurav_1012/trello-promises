// Importing 4.getCards.js
const getCards = require('../4.getCards');

const listId = '663082c6d30a82250d9b5cd7';

getCards(listId)
    .then(cardData => console.log('List Data:', cardData))
    .catch(error => console.error('Error:', error));