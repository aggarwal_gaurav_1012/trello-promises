// Importing 5.getAllCards.js
const getAllCards = require('../5.getAllCards');

const boardId = '663082c6d30a82250d9b5cd0';

getAllCards(boardId)
    .then(cardsData => console.log('Cards Data:', cardsData))
    .catch(error => console.error('Error:', error));