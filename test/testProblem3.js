// Importing 3.getLists.js
const getLists = require('../3.getLists.js');

const boardId = '663082c6d30a82250d9b5cd0';

getLists(boardId)
    .then(listData => console.log(' List Data:', listData))
    .catch(error => console.error('Error:', error));