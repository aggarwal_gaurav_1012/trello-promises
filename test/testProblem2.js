// Importing 2.createBoard.js
const createBoard = require('../2.createBoard.js');

const boardName = 'new board';

createBoard(boardName)
    .then(newBoardData => console.log('Newly Created Board Data:', newBoardData))
    .catch(error => console.error('Error:', error));