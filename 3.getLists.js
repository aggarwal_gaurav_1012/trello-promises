const fetch = require('node-fetch');
const apiKey = '302d0bd906ba164038364ba764f6f413';
const apiToken = 'ATTA71b895ec11b616f6ad63f8a7ca27a65db6ee722415ffaf86bd7665401ad3e9b47ADD2EE5';

function getLists(boardId) {
    return fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=${apiKey}&token=${apiToken}`, {
        method: 'GET',
        headers: { 'Accept': 'application/json' }
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Failed to fetch lists');
            } else {
                return response.json();
            }
        })
        .catch(error => {
            throw new Error('Failed to fetch lists');
        });
}

module.exports = getLists;
