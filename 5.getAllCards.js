const fetch = require('node-fetch');
const apiKey = '302d0bd906ba164038364ba764f6f413';
const apiToken = 'ATTA71b895ec11b616f6ad63f8a7ca27a65db6ee722415ffaf86bd7665401ad3e9b47ADD2EE5';

const getCards = require('./4.getCards');

function getAllCards(boardId) {
    return fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=${apiKey}&token=${apiToken}`, {
        method: 'GET'
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to fetch lists');
        } else {
            return response.json();
        }
    })
    .then(lists => {
        const fetchCards = lists.map(list => {
            return getCards(list.id);
        });
        return Promise.all(fetchCards);
    })
    .catch(error => {
        throw new Error('Failed to fetch cards');
    });
}

module.exports = getAllCards;