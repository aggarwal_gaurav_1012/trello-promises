const fetch = require('node-fetch');

const deleteLists = (apiKey, apiToken, listIds) => {
    const deletePromises = [];
    let sequencePromise = Promise.resolve();

    for (let listId of listIds) {
        sequencePromise = sequencePromise.then(() => {
            return fetch(`https://api.trello.com/1/lists/${listId}/closed?value=true&key=${apiKey}&token=${apiToken}`, {
                method: 'PUT'
            })
                .then(response => {
                    if (!response.ok) {
                        throw new Error(`Failed to delete list ${listId}`);
                    } else {
                        return response.json();
                    }
                })
                .then(deletedList => {
                    console.log(`List ${listId} deleted`);
                    return deletedList;
                })
                .catch(error => console.error('Error:', error));
        });
        deletePromises.push(sequencePromise);
    }
    return Promise.all(deletePromises);
};

module.exports = deleteLists;