const fetch = require('node-fetch');

const deleteLists = (apiKey, apiToken, listIds) => {
    const deletePromises = listIds.map(listId => {
        return fetch(`https://api.trello.com/1/lists/${listId}/closed?value=true&key=${apiKey}&token=${apiToken}`, {
            method: 'PUT'
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error(`Failed to delete list ${listId}`);
                } else {
                    return response.json();
                }
            })
            .catch(error => console.error('Error:', error));
    });
    return Promise.all(deletePromises);
};

module.exports = deleteLists;