const fetch = require('node-fetch');

const createBoard = (apiKey, apiToken, boardName) => {
    return fetch(`https://api.trello.com/1/boards/?name=${boardName}&key=${apiKey}&token=${apiToken}`, {
        method: 'POST'
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Failed to create board');
            } else {
                return response.json();
            }
        })
        .catch(error => console.error('Error:', error));
};

const createLists = (apiKey, apiToken, boardId, listNames) => {
    return Promise.all(listNames.map(listName => {
        return fetch(`https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${apiKey}&token=${apiToken}`, {
            method: 'POST'
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Failed to create list');
                } else {
                    return response.json();
                }
            })
            .catch(error => console.error('Error:', error));
    }));
};

const createCards = (apiKey, apiToken, listIds, cardNames) => {
    return Promise.all(listIds.map((listId, index) => {
        return fetch(`https://api.trello.com/1/cards?idList=${listId}&name=${cardNames[index]}&key=${apiKey}&token=${apiToken}`, {
            method: 'POST',
            headers: { 'Accept': 'application/json' }
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Failed to create card');
                } else {
                    return response.json();
                }
            })
            .catch(error => console.error('Error:', error));
    }));
};

module.exports = { createBoard, createLists, createCards };
