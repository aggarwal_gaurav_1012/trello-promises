const fetch = require('node-fetch');
const apiKey = '302d0bd906ba164038364ba764f6f413';
const apiToken = 'ATTA71b895ec11b616f6ad63f8a7ca27a65db6ee722415ffaf86bd7665401ad3e9b47ADD2EE5';

function getBoard(boardId) {
    const url = `https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${apiToken}`;

    return fetch(url)
        .then(response => {
            if (!response.ok) {
                throw new Error('Failed to fetch boards');
            } else {
                return response.json();
            }
        })
        .then(boards => {
            const board = boards.find(board => board.id === boardId);
            if (!board) {
                throw new Error('Board not found');
            } else {
                return board;
            }
        })
        .catch(error => console.error('Error:', error));
}

module.exports = getBoard;