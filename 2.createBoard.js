const fetch = require('node-fetch');
const apiKey = '302d0bd906ba164038364ba764f6f413';
const apiToken = 'ATTA71b895ec11b616f6ad63f8a7ca27a65db6ee722415ffaf86bd7665401ad3e9b47ADD2EE5';

function createBoard(boardName) {
    const url = `https://api.trello.com/1/boards/?name=${boardName}&key=${apiKey}&token=${apiToken}`;

    return fetch(url, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' }
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Failed to create board');
            } else {
                return response.json();
            }
        })
        .catch(error => console.error('Error:', error));
}

module.exports = createBoard;